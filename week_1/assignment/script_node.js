import * as tf from '@tensorflow/tfjs-node';

// Read data from CSV file
const readCsv = (csvUrl) =>
  tf.data.csv(csvUrl, {
    columnConfigs: {
      diagnosis: {
        isLabel: true,
      },
    },
  });

// Prepare data
const prepareData = (data, batchSize = 10) =>
  data
    .map(({ xs, ys }) => ({ xs: Object.values(xs), ys: Object.values(ys) }))
    .batch(batchSize);

const run = async function () {
  // Read and prepare training data
  const trainUrl =
    'https://gitlab.com/mwoitek/coursera-tensorflow-js/-/raw/master/week_1/assignment/data/wdbc-train.csv';
  const trainData = readCsv(trainUrl);
  const preparedTrainData = prepareData(trainData);

  // Read and prepare test data
  const testUrl =
    'https://gitlab.com/mwoitek/coursera-tensorflow-js/-/raw/master/week_1/assignment/data/wdbc-test.csv';
  const testData = readCsv(testUrl);
  const preparedTestData = prepareData(testData);

  // Model definition
  const model = tf.sequential();

  // Hidden layer 1
  const numFeatures = (await trainData.columnNames()).length - 1;
  const numHiddenUnits1 = 10;
  model.add(
    tf.layers.dense({
      inputShape: [numFeatures],
      activation: 'relu',
      units: numHiddenUnits1,
    })
  );

  // Hidden layer 2
  const numHiddenUnits2 = 10;
  model.add(
    tf.layers.dense({
      activation: 'relu',
      units: numHiddenUnits2,
    })
  );

  // Output layer
  model.add(tf.layers.dense({ activation: 'sigmoid', units: 1 }));

  // Compile model
  const learningRate = 0.05;
  model.compile({
    loss: 'binaryCrossentropy',
    optimizer: tf.train.rmsprop(learningRate),
    metrics: 'accuracy',
  });

  // Model training
  const numEpochs = 100;
  await model.fitDataset(preparedTrainData, {
    epochs: numEpochs,
    verbose: 0,
    validationData: preparedTestData,
    callbacks: {
      onEpochEnd: async (epoch, logs) =>
        console.log(
          `Epoch: ${epoch + 1} --- Loss: ${logs.loss} --- Accuracy: ${logs.acc}`
        ),
    },
  });

  // Save model
  await model.save('file://my_model');
};

run();
