// Read data from CSV file
const readCSV = (csvUrl) =>
  tf.data.csv(csvUrl, {
    columnConfigs: {
      species: {
        isLabel: true,
      },
    },
  });

// One-hot encode a label
const oneHotEncodeLabel = (ys) => [
  ys.species == 'setosa' ? 1 : 0,
  ys.species == 'virginica' ? 1 : 0,
  ys.species == 'versicolor' ? 1 : 0,
];

// Prepare data for training
const prepareData = (trainingData, batchSize) =>
  trainingData
    .map(({ xs, ys }) => ({ xs: Object.values(xs), ys: oneHotEncodeLabel(ys) }))
    .batch(batchSize);

const run = async function () {
  const trainingData = readCSV('iris.csv');
  const preparedData = prepareData(trainingData, 10);

  // Model definition
  const numFeatures = (await trainingData.columnNames()).length - 1;
  const numHiddenUnits = 5;
  const numClasses = 3;
  const learningRate = 0.06;
  const model = tf.sequential();

  // Hidden layer
  model.add(
    tf.layers.dense({
      inputShape: [numFeatures],
      activation: 'sigmoid',
      units: numHiddenUnits,
    })
  );

  // Output layer
  model.add(tf.layers.dense({ activation: 'softmax', units: numClasses }));

  model.compile({
    loss: 'categoricalCrossentropy',
    optimizer: tf.train.adam(learningRate),
  });

  // Model training
  const numEpochs = 150;
  await model.fitDataset(preparedData, {
    epochs: numEpochs,
    callbacks: {
      onEpochEnd: async (epoch, logs) =>
        console.log(`Epoch: ${epoch + 1} --- Loss: ${logs.loss}`),
    },
  });

  // Prediction
  let xsPredict = [5.5, 2.4, 3.8, 1.1];
  xsPredict = tf.tensor2d(xsPredict, [1, xsPredict.length]);
  const prediction = model.predict(xsPredict);
  alert(prediction);
};

run();
