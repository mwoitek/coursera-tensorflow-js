import * as tf from '@tensorflow/tfjs-node';

// Model definition
const model = tf.sequential();
model.add(tf.layers.dense({ units: 1, inputShape: [1] }));

model.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });

// Training data
let xsTrain = [-1, 0, 1, 2, 3, 4];
let ysTrain = [-3, -1, 1, 3, 5, 7];

xsTrain = tf.tensor2d(xsTrain, [xsTrain.length, 1]);
ysTrain = tf.tensor2d(ysTrain, [ysTrain.length, 1]);

// Model training and prediction
const numEpochs = 500;

let xsPredict = [10, 20];
xsPredict = tf.tensor2d(xsPredict, [xsPredict.length, 1]);

model
  .fit(xsTrain, ysTrain, {
    epochs: numEpochs,
  })
  .then(() => model.predict(xsPredict).print());
