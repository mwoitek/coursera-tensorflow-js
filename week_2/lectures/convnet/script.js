import { MnistData } from './data.js';

const IMG_DIM = 28;
const NUM_CLASSES = 10;
const BATCH_SIZE = 512;
const TRAIN_DATA_SIZE = 5500;
const TEST_DATA_SIZE = 1000;
const NUM_EPOCHS = 20;

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const canvasImg = document.getElementById('canvasImg');
const btnClassify = document.getElementById('btnClassify');
const btnClear = document.getElementById('btnClear');
const position = { x: 0, y: 0 };

let model;

const getModel = () => {
  const model = tf.sequential({
    layers: [
      tf.layers.conv2d({
        inputShape: [IMG_DIM, IMG_DIM, 1],
        kernelSize: 3,
        filters: 8,
        activation: 'relu',
      }),
      tf.layers.maxPool2d({ poolSize: [2, 2] }),
      tf.layers.conv2d({
        kernelSize: 3,
        filters: 16,
        activation: 'relu',
      }),
      tf.layers.maxPool2d({ poolSize: [2, 2] }),
      tf.layers.flatten(),
      tf.layers.dense({ units: 128, activation: 'relu' }),
      tf.layers.dense({ units: NUM_CLASSES, activation: 'softmax' }),
    ],
  });
  model.compile({
    optimizer: tf.train.adam(),
    loss: 'categoricalCrossentropy',
    metrics: 'accuracy',
  });
  return model;
};

const train = async (model, data) => {
  const metrics = ['loss', 'val_loss', 'acc', 'val_acc'];
  const container = { name: 'Model Training', styles: { height: '640px' } };
  const fitCallbacks = tfvis.show.fitCallbacks(container, metrics);

  const [trainXs, trainYs] = tf.tidy(() => {
    const d = data.nextTrainBatch(TRAIN_DATA_SIZE);
    return [d.xs.reshape([TRAIN_DATA_SIZE, IMG_DIM, IMG_DIM, 1]), d.labels];
  });

  const [testXs, testYs] = tf.tidy(() => {
    const d = data.nextTestBatch(TEST_DATA_SIZE);
    return [d.xs.reshape([TEST_DATA_SIZE, IMG_DIM, IMG_DIM, 1]), d.labels];
  });

  return model.fit(trainXs, trainYs, {
    batchSize: BATCH_SIZE,
    validationData: [testXs, testYs],
    epochs: NUM_EPOCHS,
    shuffle: true,
    callbacks: fitCallbacks,
  });
};

const clear = () => {
  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, 280, 280);
};

const setPosition = (mouseEvent) => {
  position.x = mouseEvent.clientX - 10;
  position.y = mouseEvent.clientY - 70;
};

const draw = (mouseEvent) => {
  if (mouseEvent.buttons != 1) return;

  ctx.lineWidth = 24;
  ctx.lineCap = 'round';
  ctx.strokeStyle = 'white';

  ctx.beginPath();
  ctx.moveTo(position.x, position.y);
  setPosition(mouseEvent);
  ctx.lineTo(position.x, position.y);
  ctx.stroke();

  canvasImg.src = canvas.toDataURL('image/png');
};

const classify = () => {
  const raw = tf.browser.fromPixels(canvasImg, 1);
  const resized = tf.image.resizeBilinear(raw, [IMG_DIM, IMG_DIM]);
  const tensor = resized.expandDims(0);

  const prediction = model.predict(tensor);
  const digit = tf.argMax(prediction, 1).dataSync();
  alert(`The digit you wrote is ${digit}`);
};

const init = () => {
  clear();

  canvas.addEventListener('mousedown', setPosition);
  canvas.addEventListener('mouseenter', setPosition);
  canvas.addEventListener('mousemove', draw);

  btnClassify.addEventListener('click', classify);
  btnClear.addEventListener('click', clear);
};

const run = async () => {
  const data = new MnistData();
  await data.load();

  model = getModel();
  tfvis.show.modelSummary({ name: 'Model Architecture' }, model);
  await train(model, data);

  init();
  alert('Training is done. Try classifying your handwritten digits!');
};

document.addEventListener('DOMContentLoaded', run);
